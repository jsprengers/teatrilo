Vue.component('scene-list', {
  template: `
            <table class="table table-bordered table-hover table-condensed">
            <thead >
            <tr>
              <th></th>
              <th scope="col">Name</th>
              <th scope="col">Duration</th>
              <th scope="col">Roles</th>
              <th scope="col">remove</th>
            </tr>
            </thead>
            <tbody>
            <tr v-for="scene in scenes" v-bind:key="scene.name">
                    <td><input class="quiet" v-model="scene.name_edit" v-on:blur="$emit('save', scene.name)"></input></td>
                    <td><input class="quiet" v-model="scene.duration_edit" size="3" v-on:blur="$emit('save', scene.name)"></input></td>
                    <td><span v-for="part in scene.parts" v-bind:key="part.value">
                    <input type="checkbox" v-model="part.selected"">
                        {{part.value}} -</span></td>
                    <td><button class="icon trash" v-on:click="$emit('remove', scene.name)"></button></td>
                    <td class="arrowButtons"><button class="icon arrow-up"  v-on:click="$emit('shift-up', scene.name)"></button>
                        <button class="icon arrow-down" v-on:click="$emit('shift-down', scene.name)"></button></td>
            </tr>
            </tbody>
            </table>
            `,
  props: ['scenes']
})

Vue.component('part-list', {
  template: `
            <table class="table table-bordered table-condensed table-hover">
              <thead >
              <tr>
                <th scope="col">Name</th>
                <th scope="col">Actor</th>
                <th scope="col">Actor</th>
                <th scope="col"></th>
              </tr>
              </thead>
              <tbody>
              <tr v-for="part in parts" v-bind:key="part.name">
                <td><input class="quiet" v-model="part.name_edit" v-on:blur="$emit('save', part.name)"></input></td>
                <td><input class="quiet" v-model="part.actor_edit" v-on:blur="$emit('save', part.name)"></input></td>
                  <td class="arrowButtons">
                  <button class="icon arrow-up" v-on:click="$emit('shift-up', part.name)"></button>
                  <button class="icon arrow-down"  v-on:click="$emit('shift-down', part.name)"></button>
                  </td>
                <td><button class="icon trash" v-on:click="$emit('remove', part.name)"></button></td>
              </tr></tbody>
            </table>`,
  props: ['parts']
})

Vue.component('rehearsal-matrix', {
  template: `
            <table class="table table-bordered table-hover">
                <thead >
                <tr><th scope="col"></th>
                    <th scope="col"></th>
                    <th scope="col" v-for="col in matrix.headerCols">
                             <div>
                               <input class="quiet" v-model="col.date_edit.day" size="2" width="30px"/>/<input class="quiet" v-model="col.date_edit.month" size="2" width="30px"/>/<input class="quiet" v-model="col.date_edit.year" size="2" width="30px"/>
                               <button class="icon update-time" v-on:click="$emit('save', col)" title="Update rehearsal date"></button>
                            </div>
                    </th>
                </tr>
                </thead>
                <tbody>
                  <tr><td></td>
                    <th scope="row">Duur</th>
                    <td v-for="col in matrix.headerCols">
                            <div><input class="quiet" v-model="col.duration_edit" size="4" v-on:blur="$emit('save', col)"></input></div>
                    </td>
                </tr>
                <tr v-for="row in matrix.rows"
                    v-bind:key="row.scene">
                    <th scope="row">{{ row.scene }}</th>
                    <td>{{ row.coverage }}</td>
                    <td v-for="cell in row.dates" v-bind:key="cell.date.toString()"  v-bind:class="{ isManual: cell.isManual }">
                        <input type="checkbox" v-model="cell.inRehearsal" v-on:click="$emit('save-scene-rehearsal-state', {date: cell.date, scene: row.scene, state: cell.inRehearsal})">
                        <input class="quiet" v-if="cell.inRehearsal" size="2"
                          v-model="cell.duration_edit"
                          v-on:blur="$emit('save-scene-rehearsal-duration', {cell: cell, scene: row.scene})"></input>
                   </td>
                </tr>
                </tbody>
            </table>
`,
  props: ['matrix']
})

Vue.component('attendance-matrix', {
  template: `
              <table class="table table-bordered table-hover">
                <thead >
                <tr>
                    <th  scope="col"></th>
                    <th  scope="col" v-for="col in matrix.headerCols">
                      <div>{{ col }}</div>
                    </th>
                </tr>
                </thead>
                <tbody>
                <tr v-for="row in matrix.rows"
                    v-bind:key="row.actor">
                    <th scope="row">{{ row.actor }}</th>
                    <td v-for="cell in row.dates">
                        {{cell.status}}
                        <input type="checkbox"
                           v-model="cell.absent"
                           v-on:click="$emit('toggle-absence', {date: cell.date, actor: row.actor, state: cell.absent})">
                   </td>
                </tr>
                </tbody>
            </table>`,
  props: ['matrix']
})

var app = new Vue({
  el: '#app',
  data: {
    production: teatrilo.production.model()
  },
  computed: {
    duration: function(){
      return teatrilo.production.totalProductionDuration()
    },
    today: function(){
      return teatrilo.today()
    },
    rehearsalMatrix: function(){
      return this.production.reCreateRehearsalMatrix()
    },
    attendanceMatrix: function(){
      return this.production.reCreateAttendanceMatrix()
    },
    settings: function(){
      return this.production.settings;
    }
  },
  methods: {
    tr: function(str){
      return teatrilo.translate(str)
    },
    updateSettings: function(settings){
      //teatrilo.production.shiftPartUp(name);
    },
    shiftPartUp: function (name) {
      teatrilo.production.shiftPartUp(name);
    },
    shiftPartDown: function (name) {
      teatrilo.production.shiftPartDown(name);
    },
    removePart: function (name) {
      teatrilo.production.removePart(name)
    },
    savePart: function (part) {
      teatrilo.production.savePart(part)
    },
    createPart: function() {
      teatrilo.production.createPart()
    },


    saveScene: function (name) {
      teatrilo.production.saveScene(name);
    },
    shiftSceneUp: function (name) {
      teatrilo.production.shiftSceneUp(name);
    },
    shiftSceneDown: function (name) {
      teatrilo.production.shiftSceneDown(name);
    },
    removeScene: function (name) {
      teatrilo.production.removeScene(name)
    },
    renameScene: function (data) {
      teatrilo.production.renameScene(data.oldName, data.newName)
    },
    createScene: function () {
      teatrilo.production.createScene()
    },


    createRehearsal: function(date){
      teatrilo.production.createRehearsal(date)
    },
    saveRehearsal: function(data){
      teatrilo.production.saveRehearsal(data)
    },
    removeRehearsal: function(date){
      teatrilo.production.removeRehearsal(date)
    },
    toggleAbsenceInRehearsal: function(data){
      if ( !data.state )
        teatrilo.production.registerAbsenceInRehearsal(data.date, data.actor)
      else
        teatrilo.production.unRegisterAbsenceInRehearsal(data.date, data.actor)
    },
    saveSceneRehearsalDuration: function(data){
      teatrilo.production.saveSceneRehearsalDuration(data)
    },
    saveSceneRehearsalState: function(data){
      teatrilo.production.saveSceneRehearsalState(data)
    }
  }
})
