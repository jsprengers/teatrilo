package storage

import production.Part
import production.Production
import rehearsal.RehearsalManager
import production.Scene
import util.NumberUtil


object ConvertToStorable {

    //TODO make sure only valid references to scene names parts and actors are serialized
    fun convert(src: Production): JSONProduction {
        val sceneNames = src.scenes.map { it.name }.toSet()
        val partNames = src.parts.map { it.name }.toSet()

        val production = JSONProduction(
            name = src.name,
            parts = src.parts.map { convertPart(it) }.toTypedArray(),
            scenes = src.scenes.map { convertScene(it) }.toTypedArray(),
            settings = JSONSettings(src.settings.rehearsalDuration,
                src.settings.rehearsalSpacing,
                src.settings.language),
            rehearsals = convertRehearsals(src.rehearsalManager, sceneNames)
        )
        return production
    }

    fun convertRehearsals(conf: RehearsalManager,
                          validScenes: Set<String>): Array<JSONRehearsal> {
        return conf.rehearsals
            .toList()
            .filter { it.date.isValid() }
            .map { rh ->
            JSONRehearsal(
                date = rh.date,
                duration = NumberUtil.coerceInt(rh.duration),
                scenes = rh.scenes.toList().map { JSONSceneRehearsal(it.scene, NumberUtil.coerceInt(it.duration)) }.toTypedArray(),
                absentees = rh.absentees
            )
        }.toTypedArray()
    }

    fun convertPart(src: Part): JSONPart = JSONPart(name = src.name, actor = src.actor)

    fun convertScene(src: Scene): JSONScene = JSONScene(name = src.name,
        duration = NumberUtil.coerceInt(src.duration),
        parts = src.parts.filter { it.selected }.map { it.value }.toTypedArray()
    )
}
