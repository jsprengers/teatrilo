package storage

import util.DateDTO

data class JSONProduction(val name: String,
                          val parts: Array<JSONPart>,
                          val scenes: Array<JSONScene>,
                          val settings: JSONSettings,
                          val rehearsals: Array<JSONRehearsal>)

data class JSONScene(val name: String,
                     val duration: Int,
                     val parts: Array<String>)

data class JSONPart(val name: String,
                    val actor: String? = null)

data class JSONSceneRehearsal(val scene: String,
                              val actualDuration: Int)

data class JSONRehearsal(val date: DateDTO,
                         val duration: Int,
                         val scenes: Array<JSONSceneRehearsal>,
                         val absentees: Array<String>)

data class JSONSettings(val rehearsalDuration: Int,
                        val rehearsalSpacing: Int,
                        val language: String)
