package storage

import production.*
import rehearsal.RehearsalManager
import rehearsal.SceneRehearsal
import util.DateDTO
import util.NumberUtil


object ConvertToModel {

    fun convert(src: JSONProduction): Production {
        val prod = Production(name = src.name,
            settings = Settings(),
            rehearsalManager = RehearsalManager(arrayOf()))
        src.parts.forEach { prod.createPart(Part(it.name, it.actor?:"")) }
        src.scenes.forEach { js ->
            val scene = Scene(
                name = js.name,
                duration = NumberUtil.coerceInt(js.duration)
            )
            src.parts.forEach { scene.addPart(it.name) }
            js.parts.forEach { scene.registerPart(it) }
            prod.createScene(scene)
        }
        val sceneNames = src.scenes.map { it.name }.toSet()
        prod.rehearsalManager.rehearsals = createRehearsals(sceneNames, src.rehearsals)
        return prod
    }

    fun createRehearsals(scenes: Set<String>, rehearsals: Array<JSONRehearsal>): Array<Rehearsal> {
        return rehearsals
            .filter { DateDTO.clone(it.date).isValid() }
            .map { rh ->
                Rehearsal(
                    date = DateDTO.clone(rh.date),
                    duration = NumberUtil.coerceInt(rh.duration),
                    scenes = rh.scenes.filter { scenes.contains(it.scene) }.map { SceneRehearsal(it.scene, NumberUtil.coerceInt(it.actualDuration)) }.toTypedArray(),
                    absentees = rh.absentees
                )
            }.toTypedArray()
    }

}
