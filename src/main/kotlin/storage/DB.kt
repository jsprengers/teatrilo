package storage

import production.Part
import production.Production
import production.Settings
import kotlin.browser.localStorage
import kotlin.browser.window

object DB {

    val production: Production = load()
    val rehearsal = production.rehearsalManager
    private var latestSaved: String? = null

    init {
        window.setInterval({ save() }, 10000)
    }

    private fun load(): Production {
        return if (window.location.search.contains("purge"))
            dummyProduction()
        else localStorage.getItem("teatrilo-default")?.let {
            parse(it)
        }
            ?: dummyProduction()
    }

    private fun parse(content: String): Production? {
        try {
            val json = JSON.parse<JSONProduction>(content)
            return ConvertToModel.convert(json)
        } catch (e: Throwable) {
            console.error("Unable to parse stored object to valid Configuration. Using fresh item instead.")
            return null
        }
    }

    private fun serialize(config: Production): String? {
        try {
            return JSON.stringify(ConvertToStorable.convert(config))
        } catch (e: Throwable) {
            console.error("Unable to serialize Configuration. Will not save.")
            return null
        }
    }

    private fun save() {
        val str = serialize(production)
        if (str == latestSaved) {
            console.info("State has not changed. Nothing to save.")
        } else {
            str?.let {
                console.log("Saving state for teatrilo-default...")
                latestSaved = it
                localStorage.setItem("teatrilo-default", it)
            }
        }
    }
}

private fun dummyProduction(): Production {
    return Production("To play or not to play")
        .createPart(Part("Hamlet"))
        .createScene("scene 1")
}
