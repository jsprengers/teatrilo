object Constants {
    val defaultRehearsalDuration = 2 * 3600

    val sceneRehearsalDurationCorrection = 0.2f

    val minimumRehearsalDuration = 30 * 60

    val maximumSceneDuration = 2 * 3600

    val minimumSceneDuration = 60

    val MIN_REHEARSAL_DURATION = 30 * 60

}
