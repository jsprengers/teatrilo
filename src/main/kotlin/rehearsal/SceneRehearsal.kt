package rehearsal

import util.HasID

data class SceneRehearsal(var scene: String,
                          var duration: Int = 0) : HasID {
    override fun id(): String = scene
    override fun sortKey(): String = id()
}
