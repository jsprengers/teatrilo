package rehearsal

import util.DateDTO
import util.DateUtil
import util.HasID

class AttendanceMatrix(
    var rows: Array<AttendanceRow>,
    var headerCols: Array<String>
)
class AttendanceRow(var actor: String,var dates: Array<AttendanceCell>)

class AttendanceCell(var date: DateDTO,
                     var status: AttendanceStatus,var absent: Boolean)

