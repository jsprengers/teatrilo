package rehearsal

import production.Part
import production.Rehearsal
import production.Scene
import util.NumberUtil


class RehearsalMatrixFactory(
    val scenes: Array<Scene>,
    val parts: Array<Part>,
    val rehearsals: Array<Rehearsal>
) {
    fun create(): RehearsalMatrix {
        val planningResult = PlanningCalculator(scenes, parts, rehearsals).create()
        val rows = scenes.map { scene ->
            val cells = rehearsals.map {
                val sh = it.getSceneRehearsal(scene.name)
                val duration = planningResult.durationForDateAndScene(scene.name, it.date)
                RehearsalCell(
                    date = it.date,
                    inRehearsal = sh != null,
                    duration = NumberUtil.roundToFive(duration.first),
                    isManual = duration.second
                )
            }.toTypedArray()
            RehearsalRow(
                scene = scene.name,
                coverage = planningResult.sceneCoverage[scene.name] ?: 0,
                dates = cells
            )
        }.toTypedArray()
        return RehearsalMatrix(rows = rows,
            headerCols = rehearsals.map { RehearsalHeaderCol(it.date, it.duration) }.toTypedArray()
        )
    }
}
