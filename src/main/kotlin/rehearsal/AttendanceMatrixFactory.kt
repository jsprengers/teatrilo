package rehearsal

import production.Part
import production.Rehearsal
import production.Scene
import util.IDList
import util.NumberUtil


class AttendanceMatrixFactory(
    val scenes: Array<Scene>,
    val parts: Array<Part>,
    val rehearsals: Array<Rehearsal>
) {
    val sceneList = IDList<Scene>()

    fun create(): AttendanceMatrix {
        val rows = allActors().map { actor ->
            val cells = rehearsals.map { rh ->
                val required = rh.scenes.flatMap { actorsInScene(sceneByName(it.scene)) }.toSet().contains(actor)
                val absent = rh.absentees.toSet().contains(actor)
                val status = AttendanceStatus.create(absent, required)
                AttendanceCell(date = rh.date, absent = absent, status = status)
            }.toTypedArray()
            AttendanceRow(actor = actor, dates = cells)
        }.toTypedArray()
        return AttendanceMatrix(
            rows = rows,
            headerCols = rehearsals.map { it.date.dayMonth() }.toTypedArray()
        )
    }

    private fun sceneByName(scene: String): Scene =
        sceneList.byID(scenes, scene) ?: throw IllegalArgumentException("Unknown scene $scene")

    fun allActors(): Array<String> = parts.map { it.actor }.filterNotNull().toSet().toTypedArray()

    fun actorsInScene(scene: Scene): List<String> {
        fun actorForPart(part: String): String? = parts.find { it.name == part }?.actor
        return scene.parts.filter { it.selected }.map { actorForPart(it.value) }
            .filterNotNull().toSet().toList()
    }
}
