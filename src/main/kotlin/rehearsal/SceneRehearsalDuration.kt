package rehearsal

import util.DateDTO
import util.DateUtil
import util.HasID

data class PlanningCalculationResult(val items: List<SceneRehearsalDuration>,
                                     val sceneCoverage: Map<String, Int>) {
    fun durationForDateAndScene(scene: String, date: DateDTO): Pair<Int, Boolean> =
        items.find { DateUtil.equal(it.date, date) && it.scene == scene }?.let { Pair(it.duration, it.isManual) } ?: Pair(0, false)
}

data class SceneRehearsalDuration(
    val scene: String,
    val date: DateDTO,
    val duration: Int,
    val isManual: Boolean
) : HasID {
    override fun id(): String = scene

    override fun sortKey(): String = id()

}
