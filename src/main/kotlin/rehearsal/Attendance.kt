package rehearsal

import rehearsal.AttendanceStatus

data class Attendance(val name: String,
                      val status: String = AttendanceStatus.IDLE.name)
