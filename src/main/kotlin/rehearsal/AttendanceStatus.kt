package rehearsal


enum class AttendanceStatus {
    PRESENT, ABSENT, MISSING, IDLE;

    companion object {
        fun create(isAbsent: Boolean, isRequired: Boolean): AttendanceStatus {
            if (isRequired && !isAbsent)
                return PRESENT
            if (isAbsent && !isRequired)
                return ABSENT
            if (isAbsent && isRequired)
                return MISSING
            if (!isRequired && !isAbsent)
                return IDLE;
            throw IllegalStateException("Should never happen!")
        }
    }
}
