package rehearsal

import production.*
import util.DateDTO
import util.DateUtil
import util.IDList
import util.NumberUtil

data class RehearsalManager(var rehearsals: Array<Rehearsal> = arrayOf()) {

    private val rehearsalList = IDList<Rehearsal>(true)

    fun getNextRehearsalDate(
        date: DateDTO,
        offset: Int
    ): DateDTO {
        val dt = rehearsals.lastOrNull()?.date ?: date
        return DateUtil.addDays(dt, offset)
    }

    fun createRehearsal(
        date: DateDTO,
        duration: Int = Settings.defaultRehearsalDuration
    ) = createRehearsal(Rehearsal(date = date, duration = duration))

    fun createRehearsal(rehearsal: Rehearsal): Rehearsal {
        rehearsals = rehearsalList.add(rehearsals, rehearsal, throwOnDuplicate = true)
        return rehearsal
    }

    fun deleteRehearsal(date: DateDTO) {
        rehearsals = rehearsalList.removeItem(rehearsals, date.toString())
    }

    fun saveRehearsal(state: RehearsalHeaderCol): Boolean {
        return rehearsalByDate(state.date)?.let { r ->
            try {
                if (r.duration != state.duration_edit) {
                    NumberUtil.requireInRange(state.duration_edit, 5, 24 * 60)
                    r.duration = state.duration_edit
                    true
                } else if (!DateUtil.equal(r.date, state.date_edit)) {
                    if (!(state.date_edit.isValid()))
                        throw IllegalStateException("Not a valid date")
                    if (DateUtil.isInPast(state.date_edit))
                        throw IllegalStateException("Date cannot be in the past")
                    r.date = state.date_edit
                    rehearsals = rehearsalList.sort(rehearsals)
                    true
                } else {
                    false
                }
            } catch (t: Throwable) {
                state.revert()
                throw t
            }
        } ?: false

    }

    fun updateSceneName(oldName: String, newName: String) {
        rehearsals.forEach { it.updateSceneName(oldName, newName) }
    }

    fun removeScene(scene: String) {
        rehearsals.forEach { it.unRegisterScene(scene) }
    }

    fun updateActorName(oldName: String, newName: String) {
        rehearsals.forEach { it.updateActorName(oldName, newName) }
    }

    fun registerAbsentee(actor: String, date: DateDTO) {
        rehearsalByDate(date)?.let {
            it.registerAbsentee(actor)
        }
    }

    fun unRegisterAbsentee(actor: String, date: DateDTO) {
        rehearsalByDate(date)?.let {
            it.unRegisterAbsentee(actor)
        }
    }

    fun getAbsenteesForRehearsal(date: DateDTO): Array<String> = rehearsalByDate(date)?.absentees ?: arrayOf()

    fun saveSceneRehearsalState(date: DateDTO, scene: String, inRehearsal: Boolean) {
        rehearsalByDate(date)?.let { rh ->
            //if (inRehearsal != rh.sceneIsRehearsed(scene)) {
            if (!inRehearsal) {
                //console.info("registering scene")
                rh.registerScene(scene)
            } else {
                //console.info("unregistering scene")
                rh.unRegisterScene(scene)
            }
            //}
        }
    }

    fun saveSceneRehearsalDuration(scene: String, cell: RehearsalCell) {
        rehearsalByDate(cell.date)?.let { rh ->
            if (NumberUtil.isNumber(cell.duration_edit)) {
                try {
                    if (rh.nmbFlexibleScenes() == 1)
                        throw IllegalStateException("Cannot adjust duration of this scene.")
                    val maxValue = rh.duration - rh.getTotalFixedDurations()
                    NumberUtil.requireInRange(cell.duration_edit, 5, maxValue)
                } catch (t: Throwable) {
                    cell.revert()
                    throw t
                }
                rh.updateSceneDuration(scene, cell.duration_edit)
            }
        }
    }

    fun rehearsalByDate(date: DateDTO): Rehearsal? = rehearsalList.byID(rehearsals, date.toString())


}
