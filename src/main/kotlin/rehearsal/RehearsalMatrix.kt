package rehearsal

import util.DateDTO
import util.HasID

class RehearsalMatrix(
    var rows: Array<RehearsalRow>,
    var headerCols: Array<RehearsalHeaderCol>
)

class RehearsalHeaderCol(
    var date: DateDTO,
    var duration: Int
) : HasID {
    var date_edit: DateDTO = date.copy()
    var duration_edit: Int = duration

    override fun id(): String = date.toString()
    override fun sortKey(): String = date.timeInMillis().toString()
    fun revert(){
        date_edit = date
        duration_edit = duration
        console.info("Reverted header col to $duration and $date")
    }
}

class RehearsalRow(
    var scene: String,
    var coverage: Int,
    var dates: Array<RehearsalCell>
)

class RehearsalCell(
    var date: DateDTO,
    var inRehearsal: Boolean = false,
    var duration: Int,
    var isManual: Boolean = false
) {
    var duration_edit = duration
    var inRehearsal_edit = inRehearsal
    fun revert(){
        console.info("Reverted header col to $duration")
        duration_edit = duration
    }
}
