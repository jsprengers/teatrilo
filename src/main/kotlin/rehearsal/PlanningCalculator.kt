package rehearsal

import Constants
import production.Part
import production.Production
import production.Rehearsal
import production.Scene
import util.IDList
import util.NumberUtil

class PlanningCalculator(val scenes: Array<Scene>,
                         val parts: Array<Part>,
                         val rehearsals: Array<Rehearsal>) {

    private val emptyResult = PlanningCalculationResult(listOf(), mapOf())
    private val sceneList = IDList<Scene>()
    fun create(): PlanningCalculationResult {
        val totalRehearsalTime = NumberUtil.sum(rehearsals.map { it.duration })
        val durationOfScenesInProduction = NumberUtil.sum(scenes.map { it.duration })

        if ( totalRehearsalTime == 0 ) {
            console.warn("Total rehearsal time is zero. Cannot calculate planning")
            return emptyResult
        }
        //println("total rehearsal time "+totalRehearsalTime)
        //println("durationOfScenesInProduction "+durationOfScenesInProduction)
        val items = rehearsals.flatMap { getSceneDurationsForRehearsal(it) }
        val map: Map<String, Int> = scenes.map { scene ->
            val coverageMinutes = NumberUtil.sum(items.filter { it.scene == scene.name }.map {it.duration})
            //println("Coverage in minutes for scene ${scene.name} $coverageMinutes")
            val sceneWeightInProduction = scene.duration.toDouble() / durationOfScenesInProduction.toDouble()
            //println("weight in production for scene ${scene.name} $sceneWeightInProduction")
            val optimalCoverageForScene = sceneWeightInProduction * totalRehearsalTime
            //println("optimal coverage for scene ${scene.name} $optimalCoverageForScene")
            val sceneCoverage = 100 * (coverageMinutes / optimalCoverageForScene)
            //println("actual scene coverage for scene ${scene.name} $sceneCoverage")
            Pair(scene.name, sceneCoverage.toInt())
        }.toMap()
        return PlanningCalculationResult(items, map)
    }

    fun getSceneDurationsForRehearsal(rehearsal: Rehearsal): List<SceneRehearsalDuration> {
        val scenesInRehearsal = rehearsal.scenes.toMutableList()
        if (scenesInRehearsal.isEmpty())
            return listOf()
        val flexibleDurationToDistribute: Int = getFlexibleDurationToDistribute(scenesInRehearsal, rehearsal.duration)

        val scenesWithZeroTimeSpent = scenesInRehearsal.filter { it.duration == 0 }
        val cumulativeWeightedDurationScenes = NumberUtil.sum(scenesWithZeroTimeSpent.map { getWeightedDurationForScene(it) })

        val sceneDurations: List<SceneRehearsalDuration> = scenesInRehearsal.map {
            val durationMinutes = if (it.duration > 0) it.duration
            else getAdjustedDuration(
                getWeightedDurationForScene(it),
                flexibleDurationToDistribute,
                cumulativeWeightedDurationScenes
            )
            SceneRehearsalDuration(it.scene,
                rehearsal.date,
                durationMinutes, it.duration > 0)
        }
        return sceneDurations
    }

    private fun getWeightedDurationForScene(sceneRehearsal: SceneRehearsal): Int {
        val weight: Float = 1 + (Constants.sceneRehearsalDurationCorrection)
        val sceneDuration = sceneList.byID(scenes, sceneRehearsal.scene)?.duration?:0
        val adjustedDurationSeconds: Float = sceneDuration.toFloat() * weight
        return adjustedDurationSeconds.toInt()
    }

    private fun getFlexibleDurationToDistribute(scenesInRehearsal: List<SceneRehearsal>, rehearsalDurationMinutes: Int): Int {
        val scenesWithFixedDuration = scenesInRehearsal.filter { it.duration > 0 }
        val cumulativeFixedDuration: Int =  NumberUtil.sum(scenesWithFixedDuration.map { it.duration })
        return rehearsalDurationMinutes.minus(cumulativeFixedDuration)
    }

    fun getAdjustedDuration(
        weightedDurationForScene: Int,
        durationToDistributeInRehearsal: Int,
        cumulativeWeightedDuration: Int
    ): Int {
        val relativeDuration = weightedDurationForScene.toFloat() / cumulativeWeightedDuration.toFloat()
        return (durationToDistributeInRehearsal.toFloat() * relativeDuration).toInt()
    }


}
