package production

import rehearsal.RehearsalHeaderCol
import rehearsal.RehearsalMatrix
import storage.DB
import util.DateDTO

@JsName("model")
fun model(): Production = DB.production

//parts
@JsName("createPart")
fun createPart() {
    captureError { Facade.createPart() }
}

@JsName("removePart")
fun removePart(name: String) {
    captureError { Facade.removePart(name) }
}

@JsName("savePart")
fun savePart(field: String) {
    captureError { Facade.savePart(field) }
}

@JsName("shiftPartUp")
fun shiftPartUp(part: String) {
    captureError { Facade.shiftPartUp(part) }
}

@JsName("shiftPartDown")
fun shiftPartDown(part: String) {
    captureError { Facade.shiftPartDown(part) }
}

// scenes
@JsName("createScene")
fun createScene() {
    captureError { Facade.createScene() }
}

@JsName("removeScene")
fun removeScene(name: String) {
    captureError { Facade.removeScene(name) }
}

@JsName("saveScene")
fun saveScene(sceneName: String) {
    captureError { Facade.saveScene(sceneName) }
}

@JsName("shiftSceneUp")
fun shiftSceneUp(scene: String) {
    captureError { Facade.shiftSceneUp(scene) }
}

@JsName("shiftSceneDown")
fun shiftSceneDown(scene: String) {
    captureError { Facade.shiftSceneDown(scene) }
}

//rehearsals
@JsName("createRehearsal")
fun createRehearsal() {
    captureError { Facade.createRehearsal() }
}

@JsName("removeRehearsal")
fun removeRehearsal(date: DateDTO) {
    captureError { Facade.removeRehearsal(date) }
}

@JsName("saveRehearsal")
fun saveRehearsal(date: RehearsalHeaderCol) {
    captureError { Facade.saveRehearsal(date) }
}

@JsName("saveSceneRehearsalDuration")
fun saveSceneRehearsalDuration(data: dynamic) {
    captureError { Facade.saveSceneRehearsalDuration(data.scene, data.cell) }
}

@JsName("saveSceneRehearsalState")
fun saveSceneRehearsalState(data: dynamic) {
    captureError { Facade.saveSceneRehearsalState(data.date, data.scene, data.state) }
}

@JsName("registerAbsenceInRehearsal")
fun registerAbsenceInRehearsal(date: DateDTO, actor: String) {
    captureError { Facade.registerAbsenceInRehearsal(date, actor) }
}

@JsName("unRegisterAbsenceInRehearsal")
fun unRegisterAbsenceInRehearsal(date: DateDTO, actor: String) {
    captureError { Facade.unRegisterAbsenceInRehearsal(date, actor) }
}

@JsName("matrix")
fun matrix(): RehearsalMatrix = DB.production.matrix

@JsName("totalProductionDuration")
fun totalProductionDuration(): Int = Facade.totalProductionDuration()

private fun captureError(fct: () -> Unit) {
    try {
        fct.invoke()
    } catch (t: Throwable) {
        console.error("Captured error: ${t.message}")
        toastError(t.message)
    }
}

private fun toastError(text: dynamic): Unit {
    js("\$('error_toast').text(text)")
    js("\$('.toast').toast({delay: 5000})")
    js("\$('.toast').toast('show')")
}

