package production

import rehearsal.RehearsalCell
import rehearsal.RehearsalHeaderCol
import storage.DB
import util.DateDTO
import util.DateUtil

object Facade {
    fun removeScene(name: String) {
        DB.production.removeScene(name)
    }

    fun removePart(name: String) {
        DB.production.removePart(name).reCreateAttendanceMatrix()
    }

    fun createScene() = DB.production.createScene()

    fun createPart() {
        DB.production.createPart()
        DB.production.reCreateAttendanceMatrix()
    }

    fun shiftSceneUp(scene: String) = DB.production.updateScenePositionUp(scene)

    fun shiftSceneDown(scene: String) = DB.production.updateScenePositionDown(scene)

    fun savePart(part: String) {
        DB.production.savePart(part)
        DB.production.reCreateAttendanceMatrix()
    }

    fun shiftPartUp(part: String) = DB.production.updatePartPositionUp(part)

    fun shiftPartDown(part: String) = DB.production.updatePartPositionDown(part)

    fun saveScene(sceneName: String) {
        DB.production.saveScene(sceneName)
        DB.production.reCreateRehearsalMatrix()
    }

    fun totalProductionDuration(): Int = DB.production.duration()

    fun createRehearsal() {
        val settings = DB.production.settings
        val date = DB.production.rehearsalManager.getNextRehearsalDate(DateUtil.today(), settings.rehearsalSpacing)
        DB.rehearsal.createRehearsal(date, settings.rehearsalDuration)
        DB.production.reCreateRehearsalMatrix()
        DB.production.reCreateAttendanceMatrix()
    }

    fun saveRehearsal(state: RehearsalHeaderCol) {
        if (DB.rehearsal.saveRehearsal(state)) {
            DB.production.reCreateRehearsalMatrix()
            DB.production.reCreateAttendanceMatrix()
        }
    }

    fun saveSceneRehearsalState(date: DateDTO, scene: String, inRehearsal: Boolean) {
        DB.rehearsal.saveSceneRehearsalState(date, scene, inRehearsal)
        DB.production.reCreateRehearsalMatrix()
        DB.production.reCreateAttendanceMatrix()
    }

    fun saveSceneRehearsalDuration(scene: String, cell: RehearsalCell) {
        DB.rehearsal.saveSceneRehearsalDuration(scene, cell)
        DB.production.reCreateRehearsalMatrix()
    }

    fun removeRehearsal(date: DateDTO) {
        DB.rehearsal.deleteRehearsal(date)
        DB.production.reCreateRehearsalMatrix()
        DB.production.reCreateAttendanceMatrix()
    }

    fun registerAbsenceInRehearsal(date: DateDTO, actor: String) {
        DB.rehearsal.registerAbsentee(actor, date)
        DB.production.reCreateAttendanceMatrix()
    }

    fun unRegisterAbsenceInRehearsal(date: DateDTO, actor: String) {
        DB.rehearsal.unRegisterAbsentee(actor, date)
        DB.production.reCreateAttendanceMatrix()
    }

}
