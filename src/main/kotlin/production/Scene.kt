package production

import util.HasID
import util.IDList
import util.Selectable

data class Scene(
    var name: String,
    var duration: Int = 10,
    var progress: Int = 0,
    var parts: Array<Selectable> = arrayOf(),
    var durationSpent: Int = 0,
    var name_edit: String = "",
    var duration_edit: Int = 0
) : HasID {

    init {
        name_edit = name
        duration_edit = duration
    }

    override fun id(): String = name
    override fun sortKey(): String = id()
    private val partList = IDList<Selectable>()

    fun partByName(name: String): String? = partList.byID(parts, name)?.value

    fun updatePartName(oldPart: String, newPart: String): Scene {
        partList.byID(parts, oldPart)?.let { it.value = newPart }
        return this
    }

    fun addPart(part: String): Scene {
        parts = partList.add(parts, Selectable(part))
        return this
    }

    fun registerPart(part: String): Scene {
        addPart(part)
        partList.byID(parts, part)?.let { it.selected = true }
        return this
    }

    fun unRegisterPart(part: String): Scene {
        partList.byID(parts, part)?.let { it.selected = false }
        return this
    }

    fun removePart(name: String): Scene {
        parts = partList.removeItem(parts, name)
        return this
    }

    fun partCount(): Int = parts.filter { it.selected }.count()

    fun isInScene(partName: String): Boolean = partList.byID(parts, partName)?.selected ?: false

    fun validate() {
        if (name.length == 0)
            throw IllegalStateException("Scene name cannot be empty")
    }

}
