package production

import rehearsal.*
import util.*

data class Production(
    var name: String,
    var parts: Array<Part> = arrayOf(),
    var scenes: Array<Scene> = arrayOf(),
    val settings: Settings = Settings(),
    val rehearsalManager: RehearsalManager = RehearsalManager()
) : HasID {

    override fun id(): String = name
    override fun sortKey(): String = id()

    private val sceneList = IDList<Scene>()
    private val partList = IDList<Part>()

    var matrix: RehearsalMatrix = RehearsalMatrix(arrayOf(), arrayOf())
    var attendance: AttendanceMatrix = AttendanceMatrix(arrayOf(), arrayOf())

    fun sceneByName(name: String): Scene? = sceneList.byID(scenes, name)

    fun createScene(): String {
        val newName = EntityNameCreator.createForScene(scenes)
        createScene(newName)
        return newName
    }

    fun createScene(name: String): Production {
        createScene(Scene(name))
        return this
    }

    fun createScene(scene: Scene): Production {
        scene.validate()
        parts.forEach { scene.addPart(it.name) }
        scenes = sceneList.add(scenes, scene)
        return this
    }

    fun saveScene(name: String): Production {
        sceneByName(name)?.let { scene ->
            updateSceneName(name, scene.name_edit)
            updateSceneDuration(name, scene.duration_edit)
        }
        return this
    }

    fun updateScenePositionUp(name: String): Production {
        scenes = ListUtil.shiftLeft(scenes, sceneByName(name) ?: throw IllegalArgumentException("unknown scene"))
        return this
    }

    fun updateScenePositionDown(name: String): Production {
        scenes = ListUtil.shiftRight(scenes, sceneByName(name) ?: throw IllegalArgumentException("unknown scene"))
        return this
    }

    fun updateSceneName(name: String, newName: String): Production {
        updateScene(name, { it.name = newName })
        rehearsalManager.updateSceneName(name, newName)
        return this
    }

    fun updateSceneDuration(name: String, duration: Int): Production {
        updateScene(name, { it.duration = duration })
        return this
    }

    private fun updateScene(name: String, setter: (Scene) -> Unit): Production {
        sceneByName(name)?.let { scene ->
            setter.invoke(scene)
            scene.validate()
            sceneList.updateItem(scenes, name, scene)
        }
        return this
    }

    fun removeScene(name: String): Production {
        scenes = sceneList.removeItem(scenes, name)
        rehearsalManager.removeScene(name)
        return this
    }

    fun duration(): Int = NumberUtil.sum(scenes.map { it.duration })

    fun partByName(name: String): Part? = partList.byID(parts, name)

    fun createPart(part: Part): Production {
        part.validate()
        parts = partList.add(parts, part)
        scenes.forEach { it.addPart(part.name) }
        return this
    }

    fun createPart(): String {
        val newName = EntityNameCreator.createForPart(parts)
        createPart(Part(newName))
        return newName
    }

    fun removePart(name: String): Production {
        scenes.forEach { it.removePart(name) }
        parts = partList.removeItem(parts, name)
        return this
    }

    fun updatePartPositionUp(name: String): Production {
        parts = ListUtil.shiftLeft(parts, partByName(name) ?: throw IllegalArgumentException("unknown part $name"))
        return this
    }

    fun updatePartPositionDown(name: String): Production {
        parts = ListUtil.shiftRight(parts, partByName(name) ?: throw IllegalArgumentException("unknown part $name"))
        return this
    }

    fun savePart(name: String): Production {
        val part = partList.byID(parts, name) ?: throw IllegalArgumentException("Unknown part: $name")
        part.validate()
        scenes.forEach { it.updatePartName(part.name, part.name_edit) }
        part.commitChanges()
        return this
    }

    fun registerPartInScene(scene: String, part: String): Production {
        val sceneObj = sceneList.byID(scenes, scene) ?: throw IllegalArgumentException("Unknown scene: $scene")
        val partObj = partList.byID(parts, part) ?: throw IllegalArgumentException("Unknown part: $part")
        sceneObj.registerPart(partObj.name)
        return this
    }

    fun unregisterPartInScene(scene: String, part: String): Production {
        val sceneObj = sceneList.byID(scenes, scene) ?: throw IllegalArgumentException("Unknown scene: $scene")
        partList.byID(parts, part) ?: throw IllegalArgumentException("Unknown part: $part")
        sceneObj.removePart(part)
        return this
    }

    fun rehearsals(): Array<Rehearsal> = rehearsalManager.rehearsals

    fun reCreateRehearsalMatrix(): RehearsalMatrix {
        matrix = RehearsalMatrixFactory(scenes, parts, rehearsalManager.rehearsals).create()
        return matrix
    }

    fun reCreateAttendanceMatrix(): AttendanceMatrix {
        attendance = AttendanceMatrixFactory(scenes, parts, rehearsalManager.rehearsals).create()
        return attendance
    }

}
