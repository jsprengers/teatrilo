package production


class Settings(var rehearsalDuration: Int = 120,
               var rehearsalSpacing: Int = 7,
               var language: String = "nl") {
    var rehearsalDuration_edit: Int = rehearsalDuration
    var rehearsalSpacing_edit: Int = rehearsalSpacing


    companion object {
        val defaultRehearsalDuration: Int = 120
        val defaultRehearsalSpacing: Int = 7
    }
}
