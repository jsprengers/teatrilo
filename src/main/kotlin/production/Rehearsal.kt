package production

import rehearsal.SceneRehearsal
import util.*

data class Rehearsal(
    var date: DateDTO,
    var duration: Int = 120,
    var scenes: Array<SceneRehearsal> = arrayOf(),
    var absentees: Array<String> = arrayOf()
) : HasID {

    override fun id(): String = date.toString()

    override fun sortKey(): String = date.timeInMillis().toString()

    private val sceneList = IDList<SceneRehearsal>()

    fun registerScene(scene: String) {
        if (sceneList.byID(scenes, scene) == null)
            scenes = sceneList.add(scenes, SceneRehearsal(scene))
    }

    fun unRegisterScene(scene: String) {
        if (sceneList.byID(scenes, scene) != null)
            scenes = sceneList.removeItem(scenes, scene)
    }

    fun registerAbsentee(actor: String) {
        absentees = StringList.add(absentees, actor)
    }

    fun unRegisterAbsentee(actor: String) {
        absentees = StringList.removeItem(absentees, actor)
    }

    fun updateSceneDuration(scene: String, duration: Int) {
        sceneList.byID(scenes, scene)?.let {
            it.duration = duration
        }
    }

    fun updateSceneName(oldName: String, newName: String) {
        scenes.find { StringUtil.equalsIgnoreCase(it.scene, oldName) }?.let {
            console.info("Updating scene $oldName to $newName")
            it.scene = newName
        }
    }

    fun updateActorName(oldName: String, newName: String) {
        absentees = StringList.removeItem(absentees, oldName)
        absentees = StringList.add(absentees, newName)
    }

    fun getTotalFixedDurations(): Int {
        val withFixed = scenes.filter { it.duration > 0 }.map { it.duration }
        return NumberUtil.sum(withFixed)
    }

    fun nmbFlexibleScenes(): Int {
        val fixed = scenes.filter { it.duration > 0 }.count()
        return scenes.size - fixed
    }

    fun getSceneRehearsal(scene: String): SceneRehearsal? = sceneList.byID(scenes, scene)

    fun sceneIsRehearsed(scene: String): Boolean = sceneList.byID(scenes, scene) != null

    fun getDurationForScene(scene: String): Int = sceneList.byID(scenes, scene)?.duration ?: 0
}
