package production

import util.HasID

class Part(var name: String,
           var actor: String? = null) : HasID {
    var name_edit: String = name
    var actor_edit: String = actor?:""
    override fun id(): String = name
    override fun sortKey(): String = id()

    fun commitChanges(){
        name = name_edit
        actor = if ( actor_edit.isBlank()) null else actor_edit
    }

    fun validate(){
        if ( name.length == 0)
            throw IllegalStateException("Part name cannot be empty")
    }
}
