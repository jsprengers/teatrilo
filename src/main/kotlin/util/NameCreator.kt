package util


object EntityNameCreator {

    fun <T : HasID> createForScene(entities: Array<T>): String =
        create(entities, "scene")

    fun <T : HasID> createForPart(entities: Array<T>): String =
        create(entities, "part")

    private fun <T : HasID> create(entities: Array<T>, entityType: String): String {
        if (entities.isEmpty()) {
            return getLocalizedName(entityType, 1)
        }
        val nameSet: Set<String> = entities.map { it.id() }.toSet()

        return getNextName(entityType, nameSet, 1)
    }

    private fun getNextName(entityType: String, nameSet: Set<String>, i: Int): String {
        val newName = getLocalizedName(entityType, i)
        return if (nameSet.contains(newName)) getNextName(
            entityType,
            nameSet,
            i + 1
        ) else newName
    }

    fun getLocalizedName(entityType: String, index: Int): String {
        return when (entityType) {
            "scene" -> "new scene $index"
            "part" -> "new role $index"
            else -> throw IllegalStateException("Entity class unknown")
        }
    }

}


