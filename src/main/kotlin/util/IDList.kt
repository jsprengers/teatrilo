package util

class IDList<T : HasID>(val withSorting: Boolean = false) {

    fun byID(list: Array<T>, id: String): T? {
        return list.find { StringUtil.equalsIgnoreCase(id, it.id()) }
    }

    fun add(list: Array<T>, item: T, throwOnDuplicate: Boolean = false): Array<T> {
        val unsorted = if (byID(list, item.id()) != null) {
            if (throwOnDuplicate)
                throw IllegalArgumentException("Cannot add item. Name exists: ${item.id()}")
            else list
        } else list.plus(item)
        return sort(unsorted)
    }

    fun updateItem(list: Array<T>, id: String, newItem: T): Array<T> {
        list.indexOf(byID(list, id)).let {
            if (it >= 0)
                list.set(it, newItem)
        }
        return sort(list)
    }

    fun removeItem(list: Array<T>, id: String): Array<T> {
        val unsorted = list.indexOf(byID(list, id)).let {
            ListUtil.removeItemAt(list, it)
        }
        return sort(unsorted)
    }

    fun sort(list: Array<T>): Array<T> = if(!withSorting) list else list.sortedBy { it.sortKey() }.toTypedArray()
}
