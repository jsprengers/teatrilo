package util

data class DateDTO(
    var year: Int,
    var month: Int,
    var day: Int
) {
    fun timeInMillis(): Double = DateUtil.convertToDate(this).getTime()

    fun copy() = DateDTO(year, month, day)

    fun plusDays(days: Int): DateDTO = DateUtil.addDays(this, days)

    fun isValid(): Boolean = DateUtil.validate(this)

    fun dayMonth(): String = "$day/$month/$year"

    override fun toString(): String = "$year $month $day"

    companion object {
        fun clone(d: DateDTO) = DateDTO(d.year, d.month, d.day)
    }

}

