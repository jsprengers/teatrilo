package util

interface HasID {
    fun id(): String
    fun sortKey(): String
}

