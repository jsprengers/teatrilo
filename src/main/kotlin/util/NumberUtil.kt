package util

import kotlin.math.round

object NumberUtil {
    fun coerceDouble(any: Any?): Double = toNumber(any?.toString() ?: "0")
    fun coerceInt(any: Any?): Int = toNumber(any?.toString() ?: "0")
    private fun toNumber(n: String): dynamic = js("Number(n)")

    fun isNumber(n: dynamic): Boolean{
       return js("return !isNaN(parseFloat(n)) && isFinite(n)") as Boolean
    }

    fun requireInRange(n: dynamic, min: Int, max: Int){
        if (!isNumber(n))
            throw IllegalArgumentException("$n is not a valid number")
        val nmb = coerceInt(n)
        if ( nmb < min)
            throw IllegalArgumentException("Number $n must be $min or greater.")
        if ( nmb > max)
            throw IllegalArgumentException("Number $n must be $max or smaller.")
    }

    fun sum(items: Iterable<Any?>): Int =  items.sumBy { coerceInt(it) }

    fun roundToFive(n: Int): Int = if ( n == 0 ) 0 else maxOf(5, (round(n / 5.0) * 5).toInt())

}
