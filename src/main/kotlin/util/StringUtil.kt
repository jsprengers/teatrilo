package util


object StringUtil {
    fun equalsIgnoreCase(s1: String, s2: String): Boolean = s1.trim().toLowerCase() == s2.trim().toLowerCase()
}
