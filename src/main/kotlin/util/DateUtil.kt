package util

import kotlin.js.Date

object DateUtil {

    fun today(): DateDTO = convertToDateDTO(Date())

    fun isInPast(d: DateDTO): Boolean =
        convertToDate(addDays(today(), -1)).getTime() > convertToDate(d).getTime()


    fun equal(d1: DateDTO, d2: DateDTO): Boolean = d1.year == d2.year && d1.month == d2.month && d1.day == d2.day

    fun addDays(dto: DateDTO, days: Int): DateDTO {
        val diff = Date(convertToDate(dto).getTime() + (days * 86400000))
        return convertToDateDTO(diff)
    }

    fun create(yr: Int, month: Int, date: Int): DateDTO? {
        try {
            val obj = Date(year = yr, month = month - 1, day = date, hour = 0, minute = 0)
            return convertToDateDTO(obj)
        } catch (t: Throwable) {
            return null
        }
    }

    fun validate(d: DateDTO): Boolean = convertToDateSafe(d) != null

    fun convertToDateDTO(date: Date): DateDTO = DateDTO(
        year = date.getFullYear(),
        month = date.getMonth() + 1,
        day = date.getDate()
    )

    fun convertToDateSafe(d: DateDTO): Date? {
        try {
            return convertToDate(d)
        } catch (t: Throwable) {
            return null
        }
    }

    fun convertToDate(d: DateDTO): Date {
        if (d.year < 1980 || d.year > 2100)
            throw IllegalArgumentException("Invalid year ${d.year}. Must be between 1980 and 2100")
        if (d.month < 1 || d.month > 12)
            throw IllegalArgumentException("Invalid month ${d.month}. Must be between 1 and 12")
        if (d.day < 1 || d.day > 31)
            throw IllegalArgumentException("Invalid day ${d.day}. Must be between 1 and 31")
        return Date(d.year, d.month - 1, d.day)
    }

}
