package util

data class Selectable(var value: String, var selected: Boolean = false) : HasID {
    override fun id(): String = value
    override fun sortKey(): String = id()
}
