package util


object ListUtil {

    fun <T> removeItemAt(input: Array<T>, index: Int): Array<T> {
        //0,1,2,3 - 0 = 1,2,3
        //0,1,2,3 - 1 = 0,2,3
        //0,1,2,3 - 3 = 0,1,2
        return if (index == -1 || input.size == 0) input
        else if (index == 0)
            input.sliceArray(1..input.lastIndex)
        else if (index == input.lastIndex)
            input.sliceArray(0..maxOf(0, input.lastIndex - 1))
        else
            input.sliceArray(0..maxOf(0, index - 1))
                .plus(input.sliceArray(minOf(input.lastIndex, index + 1)..input.lastIndex))
    }

    fun <T> shiftLeft(input: Array<T>, item: T): Array<T> {
        val index = input.indexOf(item)
        if (index == -1)
            throw IllegalStateException("Element $input not found in list.")
        else if (index == 0 || input.size <= 1)
            return input
        else {
            val prefix = if (index == 1) arrayOf() else input.sliceArray(0..maxOf(0, index - 2))
            val postFix = if (index == input.lastIndex) arrayOf() else input.sliceArray(minOf(input.lastIndex,index + 1)..input.lastIndex)
            return prefix.plus(arrayOf<T>(item, input[index - 1])).plus(postFix)
        }
    }

    fun <T> shiftRight(input: Array<T>, item: T): Array<T> {
        val index = input.indexOf(item)
        if (index == -1)
            throw IllegalStateException("Element $input not found in list.")
        else if (index == input.lastIndex || input.size <= 1)
            return input
        else {
            val prefix = if (index == 0) arrayOf() else input.sliceArray(0..maxOf(0, index - 1))
            val postFix = if (index >= input.lastIndex - 1)  arrayOf() else input.sliceArray(minOf(input.lastIndex,index + 2) .. input.lastIndex)
            return prefix.plus(arrayOf<T>(input[index + 1], item)).plus(postFix)
        }
    }


}
