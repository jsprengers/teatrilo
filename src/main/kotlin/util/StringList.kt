package util

object StringList {

    fun byID(list: Array<String>, id: String): String? {
        return list.find { StringUtil.equalsIgnoreCase(id, it) }
    }

    fun add(list: Array<String>, item: String, throwOnDuplicate: Boolean = false): Array<String> {
        return if (byID(list, item) != null) {
            if (throwOnDuplicate)
                throw IllegalArgumentException("Cannot add item. Name exists: ${item}")
            else list
        } else list.plus(item)
    }

    fun updateItem(list: Array<String>, id: String, newItem: String): Array<String> {
        list.indexOf(byID(list, id)).let {
            if (it >= 0)
                list.set(it, newItem)
        }
        return list
    }

    fun  removeItem(list: Array<String>, id: String): Array<String> {
        return list.indexOf(byID(list, id)).let {
            ListUtil.removeItemAt(list, it)
        }
    }
}
