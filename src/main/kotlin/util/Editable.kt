package util

abstract class Editable<T>(var value: T) {
    var scratch: T = copy(value)
    abstract fun copy(src: T): T
}

class DateField(src: DateDTO) : Editable<DateDTO>(src){
    override fun copy(src: DateDTO) = src.copy()
}

class StringField(src: String) : Editable<String>(src){
    override fun copy(src: String) = src
}

class NumberField(src: Int) : Editable<Int>(src){
    override fun copy(src: Int) = src
}
