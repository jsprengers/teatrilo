abstract class I18NResource(pairs: List<String>) {
    private fun split(str: String): Pair<String, String> {
        val elements = str.split(":")
        return if (elements.size != 2)
            Pair(str, str)
        else Pair(elements[0], elements[1])
    }

    private val map: Map<String, String> = pairs.map { split(it) }.toMap()
    operator fun get(str: String): String = map.get(str) ?: str
}

object NL : I18NResource(
    listOf(
        "tab_rehearsals:Repetitiekalender",
        "tab_production:Scenes and cast",
        "tab_settings:Voorkeuren",
        "rh_calendar_and_planning:Repetitiekalender en planning",
        "rh_add_rehearsal:Voeg repetitie toe",
        "rh_absentees:Absenties en bezetting",
        "parts_header:Rolverdeling",
        "parts_add_button:Voeg een rol toe",
        "scenes_add_button:Voeg een scene toe",
        "settings_header:Voorkeuren",
        "settings_language_help:Kies Nederlands of Engels",
        "settings_language_field:Taal",
        "settings_rh_field:Standaard repetitieduur",
        "settings_rh_help:Geef hier op hoe lang een normale repetitie duurt. Je kunt het later aanpassen voor een specifieke datum.",
        "rh_spacing_field:Dagen tussen repetities",
        "rh_spacing_help:Dit aantal dagen wordt opgeteld na de laatste repetitie wanneer je een nieuwe toevoegt."
    )
)

object EN : I18NResource(
    listOf(
        "tab_rehearsals:Rehearsal calendar",
        "tab_production:Scenes and cast",
        "tab_settings:Settings",
        "rh_calendar_and_planning:Rehearsal calendar and planning",
        "rh_add_rehearsal:Add a rehearsal",
        "rh_absentees:Absentees",
        "parts_header:Roles and cast",
        "parts_add_button:Add a role and casting",
        "scenes_add_button:Add a scene",
        "settings_header:Settings",
        "settings_language_help:Choose Dutch of English",
        "settings_language_field:Language",
        "settings_rh_field:Standard rehearsal duration",
        "settings_rh_help:Enter the normal rehearsal duration in minutes. You can always change it for a specific date.",
        "rh_spacing_field:Days between rehearsals",
        "rh_spacing_help:This is the number of days added to the last rehearsal, convenient when adding a new one."
    )
)
