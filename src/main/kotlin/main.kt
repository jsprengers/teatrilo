import storage.DB
import util.DateDTO
import util.DateUtil
import util.StringUtil

fun main() {

}

@JsName("translate")
fun translate(str: String): String {
    val impl = when (DB.production.settings.language) {
        "nl" -> NL
        "en" -> EN
        else -> throw IllegalStateException("Unsupported language: $str")
    }
    return impl.get(str)
}

@JsName("today")
fun today(): DateDTO = DateUtil.today()
