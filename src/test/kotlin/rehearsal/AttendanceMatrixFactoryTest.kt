package rehearsal

import production.Part
import production.Rehearsal
import production.Scene
import util.DateDTO
import util.Selectable
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertTrue

class AttendanceMatrixFactoryTest {
    val now = DateDTO(2020,5,1)
    val next = DateDTO(2020,5,8)

    val claudius = Part("Claudius", "John")
    val ghost = Part("Ghost", "John")
    val hamlet = Part("Hamlet", "Nathan")
    val ophelia = Part("Ophelia", "Helen")
    val polonius = Part("Polonius", "Dick")
    val sc1 = Scene("s1")
    val sc2 = Scene("s2")
    val r1 = Rehearsal(now, duration = 120)
    val r2 = Rehearsal(now, duration = 120)

    @Test
    fun actorsInScene() {
        val partsInScene = arrayOf(
            Selectable("Ghost", true),
            Selectable("Claudius", true),
            Selectable("Ophelia", true)
        )
        val scene1 = Scene("S1", parts = partsInScene)
        val factory = AttendanceMatrixFactory(arrayOf(scene1), arrayOf(claudius, ghost, hamlet, ophelia), arrayOf(r1, r2))
        val actors = factory.actorsInScene(scene1).toSet()
        assertEquals(2, actors.size)
        assertTrue(actors.contains("John"))
        assertTrue(actors.contains("Helen"))
    }

    @Test
    fun attendance(){
        // Helen (Ophelia) is not needed and present
        // John (Claudius and Ghost) is needed and present
        // Dick (Polonius) is absent and not needed
        // Nathan (Hamlet) is absent and needed
        sc1.registerPart("Claudius")
        sc1.registerPart("Ghost")
        sc1.registerPart("Hamlet")
        r1.registerAbsentee("Dick")
        r1.registerAbsentee("Nathan")
        r1.registerScene(sc1.name)
        val factory = AttendanceMatrixFactory(arrayOf(sc1),
            arrayOf(claudius, ghost, hamlet, ophelia, polonius),
            arrayOf(r1))
        val statusPerActor = factory.create().rows.map { Pair(it.actor, it.dates[0].status)  }.toMap()
        assertEquals("IDLE", statusPerActor["Helen"].toString())
        assertEquals("PRESENT", statusPerActor["John"].toString())
        assertEquals("ABSENT", statusPerActor["Dick"].toString())
        assertEquals("MISSING", statusPerActor["Nathan"].toString())
    }

    @Test
    fun getAllActors(){
        val factory = AttendanceMatrixFactory(arrayOf(sc1, sc2), arrayOf(claudius, ghost, hamlet, ophelia), arrayOf(r1, r2))
        val actors = factory.allActors().toSet()
        assertEquals(3, actors.size)
        assertTrue(actors.contains("John"))
        assertTrue(actors.contains("Nathan"))
        assertTrue(actors.contains("Helen"))
    }


}
