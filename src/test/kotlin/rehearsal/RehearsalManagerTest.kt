package rehearsal

import production.Rehearsal
import util.DateDTO
import kotlin.test.*


class RehearsalManagerTest {

    val today = DateDTO(2020, 1, 10)
    val nextWeek = DateDTO(2020, 1, 17)
    val weekAfterNext = DateDTO(2020, 1, 24)
    val conf = RehearsalManager()

    @Test
    fun createAndRetrieveRehearsal(){
        conf.createRehearsal(nextWeek)
        assertEquals(nextWeek, conf.rehearsalByDate(nextWeek)!!.date)
    }

    @Test
    fun testNextRehearsalDate(){
        assertEquals(13, conf.getNextRehearsalDate(today, 3).day)
    }

    @Test
    fun testNextRehearsalWhenTwoPresent(){
        conf.createRehearsal(weekAfterNext)
        conf.createRehearsal(today)
        assertEquals(27, conf.getNextRehearsalDate(today, 3).day)
    }

    @Test
    fun cannotCreateRehearsalOnExactSameTime(){
        conf.createRehearsal(nextWeek)
        assertFails { conf.createRehearsal(nextWeek) }
    }

    @Test
    fun removeRehearsal(){
        conf.createRehearsal(today)
        conf.createRehearsal(nextWeek)
        conf.deleteRehearsal(today)
        assertEquals(1, conf.rehearsals.size)
    }

    @Test
    fun updateRehearsalDuration(){
        conf.createRehearsal(Rehearsal(today, 130))
        assertEquals(130, conf.rehearsalByDate(today)!!.duration)
    }

    @Test
    fun testSorting(){
        conf.createRehearsal(weekAfterNext)
        conf.createRehearsal(nextWeek)
        conf.createRehearsal(today)
        assertEquals(today, conf.rehearsals[0].date)
        assertEquals(nextWeek, conf.rehearsals[1].date)
        assertEquals(weekAfterNext, conf.rehearsals[2].date)
    }

  /*  @Test
    fun updateRehearsalDate(){
        val rh = conf.createRehearsal(Rehearsal(today, 130))
        rh.registerScene("S1")
        conf.saveSceneRehearsal("S1", today, 11)
        //TODO validate absentee
        assertEquals(1, conf.rehearsals.size)
        conf.saveRehearsal(RehearsalState())
        assertEquals(1, conf.rehearsals.size)
        //make sure the original 130 minutes is copied, as well as S1
        assertEquals(130, conf.rehearsalByDate(nextWeek)!!.duration)
        assertEquals(11, conf.rehearsalByDate(nextWeek)!!.durationForScene("S1"))
    }*/
/*
    @Test
    fun registerSceneInRehearsal(){
        conf.createRehearsal(today)

        registerSceneInRehearsal(today, "S1")
        registerSceneInRehearsal(today, "S2")
        assertTrue(conf.rehearsalByDate(today)!!.sceneIsRehearsed("S1"))
        assertTrue(conf.rehearsalByDate(today)!!.sceneIsRehearsed("S2"))
        unRegisterSceneInRehearsal(today, "S1")
        assertFalse(conf.rehearsalByDate(today)!!.sceneIsRehearsed("S1"))
        assertTrue(conf.rehearsalByDate(today)!!.sceneIsRehearsed("S2"))
    }*/

    @Test
    fun updateSceneDurationInRehearsal(){
        val r = conf.createRehearsal(today)
        r.registerScene("S1")
        r.updateSceneDuration("S1", 11)
        assertEquals(11, conf.rehearsalByDate(today)!!.getDurationForScene("S1"))
    }

    @Test
    fun registerAbsentee(){
        conf.createRehearsal(today)
        conf.registerAbsentee("John", today)
        assertEquals(1, conf.getAbsenteesForRehearsal(today).size)
        conf.unRegisterAbsentee("John", today)
        assertEquals(0, conf.getAbsenteesForRehearsal(today).size)
    }

    @Test
    fun updateSceneName(){
        val r = conf.createRehearsal(today)
        r.registerScene("S1")
        r.registerScene("S2")
        val r2 = conf.createRehearsal(nextWeek)
        r2.registerScene("S1")
        r2.registerScene("S2")
        conf.updateSceneName("S1", "Scene one")

        assertEquals(11, conf.rehearsalByDate(today)!!.getDurationForScene("Scene one"))
        assertEquals(12, conf.rehearsalByDate(nextWeek)!!.getDurationForScene("Scene one"))
    }

    @Test
    fun updateActorName(){
        conf.createRehearsal(today)
        conf.createRehearsal(nextWeek)
        conf.registerAbsentee("John", today)
        conf.registerAbsentee("John", nextWeek)
        conf.updateActorName("John", "Nate")
        assertEquals("Nate", conf.getAbsenteesForRehearsal(today)[0])
        assertEquals("Nate", conf.getAbsenteesForRehearsal(nextWeek)[0])
    }


}
