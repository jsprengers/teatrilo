package rehearsal

import production.Part
import production.Rehearsal
import production.Scene
import util.DateDTO
import util.Selectable
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertTrue

class RehearsalMatrixFactoryTest {
    val now = DateDTO(2020,5,1)
    val next = DateDTO(2020,5,8)

    val claudius = Part("Claudius", "John")
    val ghost = Part("Ghost", "John")
    val hamlet = Part("Hamlet", "Nathan")
    val ophelia = Part("Ophelia", "Helen")
    val polonius = Part("Polonius", "Dick")
    val sc1 = Scene("s1")
    val sc2 = Scene("s2")
    val r1 = Rehearsal(now, duration = 120)
    val r2 = Rehearsal(now, duration = 120)


}
