package util

import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertFails


class NumberUtilTest {

    @Test
    fun testNumberCoercionForZero(){
        assertEquals(0.0, NumberUtil.coerceDouble(null))
        assertEquals(0.0, NumberUtil.coerceDouble("0"))
        assertEquals(0.0, NumberUtil.coerceDouble(""))
        assertEquals(0.0, NumberUtil.coerceDouble(0))
    }

    @Test
    fun testNumberCoercion(){
        assertEquals(42.42, NumberUtil.coerceDouble(42.42))
        assertEquals(42.0, NumberUtil.coerceDouble("42"))
    }
    @Test
    fun testSum(){
        assertEquals(10, NumberUtil.sum(listOf("1",5,"0","4")))
        assertEquals(0, NumberUtil.sum(listOf()))
    }

    @Test
    fun testValidRanges(){
        NumberUtil.requireInRange("5", 5, 10)
        NumberUtil.requireInRange("10", 5, 10)
        assertFails { NumberUtil.requireInRange(0, 5, 10) }
        assertFails { NumberUtil.requireInRange(4, 5, 10) }
        assertFails { NumberUtil.requireInRange(11, 5, 10) }
        assertFails { NumberUtil.requireInRange(null, 5, 10) }
        assertFails { NumberUtil.requireInRange("", 5, 10) }
        assertFails { NumberUtil.requireInRange("!@#", 5, 10) }
    }

    @Test
    fun roundToFive(){
        assertEquals(0, NumberUtil.roundToFive(0))
        assertEquals(5, NumberUtil.roundToFive(1))
        assertEquals(5, NumberUtil.roundToFive(7))
        assertEquals(10, NumberUtil.roundToFive(8))
        assertEquals(10, NumberUtil.roundToFive(12))
    }
}
