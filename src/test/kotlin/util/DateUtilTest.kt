package util

import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertFalse
import kotlin.test.assertTrue


class DateUtilTest {

    @Test
    fun testToday(){
        val today = DateUtil.today()
        assertTrue(today.year >= 2020)
    }

    @Test
    fun testLeapYear(){
        val today = DateUtil.create(2020, 2, 28)
        val leapDay = DateUtil.addDays(today!!, 2)
        assertEquals(3, leapDay.month)
        assertEquals(1, leapDay.day)
    }

    @Test
    fun testRegularYear(){
        val today = DateUtil.create(2019, 2, 28)
        val leapDay = DateUtil.addDays(today!!, 2)
        assertEquals(3, leapDay.month)
        assertEquals(2, leapDay.day)
    }

    @Test
    fun validate(){
        assertFalse(DateUtil.validate(DateDTO(2020,13,1)))
        assertFalse(DateUtil.validate(DateDTO(2020,0,1)))
        assertFalse(DateUtil.validate(DateDTO(2020,1,0)))
    }


}
