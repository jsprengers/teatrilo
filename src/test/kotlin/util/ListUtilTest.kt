package util

import kotlin.test.Test
import kotlin.test.assertEquals


class ListUtilTest {

    @Test
    fun testWithEmptyList(){
        assertEquals(0, ListUtil.removeItemAt(arrayOf<String>(), 0).size)
    }

    @Test
    fun removeOnlyElement(){
        val smaller = ListUtil.removeItemAt(arrayOf<String>("one"), 0)
        assertEquals(0, smaller.size)
    }

    @Test
    fun removeFirstElement(){
        val smaller = ListUtil.removeItemAt(arrayOf<String>("one", "two"), 0)
        assertEquals(1, smaller.size)
        assertEquals("two", smaller[0])
    }

    @Test
    fun removeLastElement(){
        val smaller = ListUtil.removeItemAt(arrayOf<String>("one", "two"), 1)
        assertEquals(1, smaller.size)
        assertEquals("one", smaller[0])
    }

    @Test
    fun removeMiddleElement(){
        val smaller = ListUtil.removeItemAt(arrayOf<String>("one", "two", "three"), 1)
        assertEquals(2, smaller.size)
        assertEquals("one", smaller[0])
        assertEquals("three", smaller[1])
    }

    @Test
    fun testShiftLeftInFirstPostionDoesNothing(){
        assertList(ListUtil.shiftLeft(arrayOf("a", "b", "c"), "a"), "a", "b", "c")
    }

    @Test
    fun testShiftLeftInSecondPosition(){
        assertList(ListUtil.shiftLeft(arrayOf("a", "b", "c"), "b"), "b", "a", "c")
    }

    @Test
    fun testShiftLeftInLastPosition(){
        assertList(ListUtil.shiftLeft(arrayOf("a", "b", "c"), "c"), "a", "c", "b")
    }

    @Test
    fun twoItemsShiftLeftInFirstPostionDoesNothing(){
        assertList(ListUtil.shiftLeft(arrayOf("a", "b"), "a"), "a", "b")
    }

    @Test
    fun twoItemsShiftLeftInSecondPosition(){
        assertList(ListUtil.shiftLeft(arrayOf("a", "b"), "b"), "b", "a")
    }

    @Test
    fun oneItemShiftDoesNothing(){
        assertList(ListUtil.shiftLeft(arrayOf("a"), "a"), "a")
    }

    @Test
    fun shiftLeftAllTheWay(){
        val start = arrayOf("a", "b", "c", "d")
        val it1 = ListUtil.shiftLeft(start, "d")
        val it2 = ListUtil.shiftLeft(it1, "d")
        val it3 = ListUtil.shiftLeft(it2, "d")
        assertList(it3, "d", "a", "b", "c")
    }


    // test shift right
    @Test
    fun testShiftRightInLastPostionDoesNothing(){
        assertList(ListUtil.shiftRight(arrayOf("a", "b", "c"), "c"), "a", "b", "c")
    }

    @Test
    fun testShiftRightInSecondPosition(){
        assertList(ListUtil.shiftRight(arrayOf("a", "b", "c"), "b"), "a", "c", "b")
    }

    @Test
    fun testShiftRightInFirstPosition(){
        assertList(ListUtil.shiftRight(arrayOf("a", "b", "c"), "a"), "b", "a", "c")
    }

    @Test
    fun twoItemsShiftRightInLastPostionDoesNothing(){
        assertList(ListUtil.shiftRight(arrayOf("a", "b"), "b"), "a", "b")
    }

    @Test
    fun twoItemsShiftRightInFirstPosition(){
        assertList(ListUtil.shiftRight(arrayOf("a", "b"), "a"), "b", "a")
    }

    @Test
    fun shiftRightAllTheWay(){
        val start = arrayOf("a", "b", "c", "d")
        val it1 = ListUtil.shiftRight(start, "a")
        val it2 = ListUtil.shiftRight(it1, "a")
        val it3 = ListUtil.shiftRight(it2, "a")
        assertList(it3, "b", "c", "d", "a")
    }

    @Test
    fun oneItemShiftRightDoesNothing(){
        assertList(ListUtil.shiftRight(arrayOf("a"), "a"), "a")
    }

    private fun assertList(input: Array<String>, vararg check: String){
        IntRange(0, check.size).forEach { i ->
            assertEquals(check[i], input[i], "$check / $input")
        }
    }
}
