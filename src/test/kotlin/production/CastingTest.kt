package production

import kotlin.test.*


class CastingTest {

    val production = Production(name = "Hamlet")
    @BeforeTest
    fun setup(){
        production.createScene("scene 1")
        production.createScene("scene 2")
    }

    @Test
    fun addNewParts(){
        production.createPart(Part("Hamlet"))
        assertNotNull(production.partByName("Hamlet"))
    }

    @Test
    fun updatePartName(){
        val part = Part("hamlet")
        production.createPart(part)
        part.name_edit = "Hamlet, Prince of Denmark"
        production.savePart("hamlet")
        assertNotNull(production.partByName("Hamlet, Prince of Denmark"))
    }

    @Test
    fun updateActorForPart(){
        val part = Part("Gertrude")
        production.createPart(part)
        part.actor_edit = "Helena Bonham Carter"
        production.savePart("Gertrude")
        assertEquals("Helena Bonham Carter", production.partByName("Gertrude")!!.actor)
    }

    @Test
    fun updateActorToBlankSetsNull(){
        val part = Part("Gertrude")
        production.createPart(part)
        part.actor_edit = ""
        production.savePart("Gertrude")
        assertNull(production.partByName("Gertrude")!!.actor)
    }


}
