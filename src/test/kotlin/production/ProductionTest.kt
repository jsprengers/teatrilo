package production

import util.DateDTO
import kotlin.test.*


class ProductionTest {

    val production = Production(name = "Hamlet")

    @Test
    fun addNewScene(){
        production.createScene("scene 1")
        assertNotNull(production.sceneByName("scene 1"))
    }

    @Test
    fun addSceneWithSameNameDoesNothing(){
        production.createScene("scene 1")
        production.createScene("Scene 1")
        assertEquals(1, production.scenes.size)
    }

    @Test
    fun updateSceneName(){
        production.createScene("scene 1")
        production.updateSceneName("scene 1", "Scene one")
        assertEquals(1, production.scenes.size)
        assertNull(production.sceneByName("scene 1"))
    }

    @Test
    fun updateSceneDuration(){
        val scene = Scene("scene 1", duration = 20)
        production.createScene(scene)
        production.updateSceneName("scene 1", "scene 1")
        production.updateSceneDuration("scene 1", 22)
        assertEquals(22, production.sceneByName("scene 1")!!.duration)
    }

    @Test
    fun totalProductionDuration(){
        production.createScene(Scene("scene 1", duration = 10))
        production.createScene(Scene("scene 2", duration = 10))
        production.createScene(Scene("scene 3", duration = 13))
        assertEquals(33, production.duration())
        production.updateSceneName("scene 2", "scene 1")
        production.updateSceneDuration("scene 1", 12)
        assertEquals(35, production.duration())
    }

    @Test
    fun updatePartName(){
        val newName="Hamlet, Prince of Denmark"
        val part = Part("Hamlet")
        production.createPart(part)
            .createScene("scene 1")
            .createScene("scene 2")
            .registerPartInScene("scene 1", "Hamlet")
            .registerPartInScene("scene 2", "Hamlet")
        part.name_edit = newName
        production.savePart("Hamlet")
        assertEquals(newName, production.parts[0].name)
        assertEquals(newName, production.scenes[0].parts[0].value)
        assertEquals(newName, production.scenes[1].parts[0].value)
    }

    @Test
    fun removePartFromScene(){
        production.createPart(Part("Hamlet")).createPart(Part("Gertrude"))
            .createScene("scene 1")
            .registerPartInScene("scene 1", "Hamlet")
            .registerPartInScene("scene 1", "Gertrude")
        production.unregisterPartInScene("scene 1", "Gertrude")
        assertEquals(1, production.scenes[0].parts.size)
    }

    @Test
    fun removePart(){
        production.createPart(Part("Hamlet")).createPart(Part("Gertrude"))
            .createScene("scene 1")
            .registerPartInScene("scene 1", "Hamlet")
            .registerPartInScene("scene 1", "Gertrude")
        production.removePart("Gertrude")
        assertEquals(1, production.scenes[0].parts.size)
    }

    @Test
    fun removeScene(){
        production.createPart(Part("Hamlet")).createPart(Part("Gertrude"))
            .createScene("scene 1").createScene("scene 2")
        production.removeScene("scene 1")
        assertEquals("scene 2", production.scenes[0].name)
    }


    @Test
    fun updateActorInPart(){
        val tony="Anthony Hopkins"
        val michael="Michael Gambon"
        production.createPart(Part("Ghost", tony)).createPart(Part("Claudius", tony))
        assertEquals(tony, production.partByName("Ghost")!!.actor)
        assertEquals(tony, production.partByName("Claudius")!!.actor)
        production.partByName("Ghost")!!.actor_edit = michael
        production.savePart("Ghost")
        assertEquals(michael, production.partByName("Ghost")!!.actor)
        assertEquals(tony, production.partByName("Claudius")!!.actor)
    }


    @Test
    fun testMatrix(){
        val today = DateDTO(2020, 1, 10)
        val nextWeek = DateDTO(2020, 1, 17)
        val weekAfterNext = DateDTO(2020, 1, 24)
        production.createScene("S1")
        production.createScene("S2")
        production.createScene("S3")
        val manager = production.rehearsalManager
        val rToday = manager.createRehearsal(Rehearsal(today, 120))
        val rNextweek = manager.createRehearsal(Rehearsal(nextWeek, 120))
        val rWeekAfterNext = manager.createRehearsal(Rehearsal(weekAfterNext, 120))
        rToday.registerScene("S1")
        rToday.registerScene("S2")
        rToday.registerScene("S3")
        rNextweek.registerScene("S2")
        rNextweek.registerScene("S3")
        rNextweek.registerScene("S3")

        rWeekAfterNext.registerScene("S3")
        val matrix = production.reCreateRehearsalMatrix()
        assertEquals(3, matrix.rows.size)
        //row for scene 1
        assertTrue(matrix.rows[0].dates[0].inRehearsal)
        assertFalse(matrix.rows[0].dates[1].inRehearsal)
        assertFalse(matrix.rows[0].dates[2].inRehearsal)
        //row for scene 2
        assertTrue(matrix.rows[1].dates[0].inRehearsal)
        assertTrue(matrix.rows[1].dates[1].inRehearsal)
        assertFalse(matrix.rows[1].dates[2].inRehearsal)
        //row for scene 3
        assertTrue(matrix.rows[2].dates[0].inRehearsal)
        assertTrue(matrix.rows[2].dates[1].inRehearsal)
        assertTrue(matrix.rows[2].dates[2].inRehearsal)
    }



}
