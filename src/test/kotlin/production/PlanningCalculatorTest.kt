package production

import rehearsal.PlanningCalculator
import util.DateDTO
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertTrue


class PlanningCalculatorTest {

    val now = DateDTO(2020,6,1)
    val scenes = arrayOf(Scene("scene 1", 10 ), Scene("scene 2", 20 ), Scene("scene 3", 40 ))

    @Test
    fun testAdjustedRatiosWithNoManualChanges() {
        val rehearsal = Rehearsal(now, duration = 140)
        val service = PlanningCalculator(scenes, arrayOf(), arrayOf(rehearsal))

        rehearsal.registerScene("scene 1")
        rehearsal.registerScene("scene 2")
        rehearsal.registerScene("scene 3")
        //production.rehearsalConf.updateSceneDuration("scene 3", now, 42 )

        val result = service.create()
        assertEquals(20 , result.durationForDateAndScene("scene 1", now).first)
        assertEquals(40 , result.durationForDateAndScene("scene 2", now).first)
        assertEquals(80 , result.durationForDateAndScene("scene 3", now).first)
        //assertTrue(result.durationForDateAndScene("scene 4", now).second)
    }

    @Test
    fun testAdjustedRatiosWithManualChanges() {
        val rehearsal = Rehearsal(now, duration = 140)
        val service = PlanningCalculator(scenes, arrayOf(), arrayOf(rehearsal))

        rehearsal.registerScene("scene 1")
        rehearsal.registerScene("scene 2")
        rehearsal.registerScene("scene 3")
        rehearsal.updateSceneDuration("scene 3", 90)

        val result = service.create()
        assertEquals(16 , result.durationForDateAndScene("scene 1", now).first)
        assertEquals(33 , result.durationForDateAndScene("scene 2", now).first)
        assertEquals(90 , result.durationForDateAndScene("scene 3", now).first)
        assertTrue(result.durationForDateAndScene("scene 3", now).second)
        //assertTrue(result.durationForDateAndScene("scene 4", now).second)
    }

    @Test
    fun getAdjustedDuration() {
        val service = PlanningCalculator(scenes, arrayOf(), arrayOf())
        assertEquals(15 , service.getAdjustedDuration(
            10 ,
            30 ,
            20 ))
    }

}
