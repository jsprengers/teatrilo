package production

import production.Production
import production.Scene
import kotlin.test.*


class SceneTest {

    val production = Production(name = "Hamlet")

    @Test
    fun addPartToScene() {
        val scene = Scene("scene 1")
        scene.registerPart("Hamlet")
        scene.registerPart("Gertrude")
        scene.registerPart("Ghost")
        assertEquals(3, scene.partCount())
        assertEquals("Hamlet", scene.partByName("Hamlet"))
    }

    @Test
    fun hasTwoPartsInScene(){
        val scene = Scene("scene 1")
        scene.registerPart("Hamlet")
        scene.registerPart("Gertrude")
        scene.addPart("Ghost")
        assertEquals(2, scene.partCount())
    }

    @Test
    fun addingPartTwiceDoesNothing() {
        val scene = Scene("scene 1")
        scene.registerPart("Hamlet")
        scene.registerPart("Hamlet")
        assertEquals(1, scene.partCount())
    }

    @Test
    fun removePartFromScene() {
        val scene = Scene("scene 1")
        scene.registerPart("Hamlet")
        scene.registerPart("Gertrude")
        scene.registerPart("Ghost")
        scene.removePart("Gertrude")
        assertEquals(2, scene.partCount())
    }

    @Test
    fun unregisterPart() {
        val scene = Scene("scene 1")
        scene.registerPart("Hamlet")
        scene.registerPart("Gertrude")
        assertTrue(scene.isInScene("Gertrude"))
        assertEquals(2, scene.partCount())
        scene.unRegisterPart("Gertrude")
        assertEquals(1, scene.partCount())
        assertFalse(scene.isInScene("Gertrude"))
    }


}
