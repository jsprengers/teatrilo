package production

import util.EntityNameCreator
import kotlin.test.Test
import kotlin.test.assertEquals


class EntityNameCreatorTest {

    @Test
    fun createFirstScene(){
        assertEquals("new scene 1", EntityNameCreator.createForScene(arrayOf()))
    }

    @Test
    fun createThirdScene(){
        assertEquals("new scene 3", EntityNameCreator.createForScene(arrayOf(Scene("new scene 1"), Scene("new scene 2"))))
    }

    @Test
    fun createCustomName(){
        assertEquals("new scene 1", EntityNameCreator.createForScene(arrayOf(Scene("To be or not to be"))))
    }
}
