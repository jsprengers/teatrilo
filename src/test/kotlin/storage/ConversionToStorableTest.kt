package storage

import production.*
import util.DateDTO
import util.Selectable
import kotlin.test.Test
import kotlin.test.assertEquals


class ConversionToStorableTest {

    @Test
    fun convertToStorable(){
        val date = DateDTO(2010, 1, 1)
        val production = Production("Hamlet")
            .createPart(Part("Hamlet", "John"))
            .createPart(Part("Ophelia", "Mary"))
            .createScene("Scene 1")
            .createScene("Scene 2")
            .registerPartInScene("Scene 1", "Hamlet")
            .registerPartInScene("Scene 2", "Hamlet")
            .registerPartInScene("Scene 2", "Ophelia")
        val rh = production.rehearsalManager.createRehearsal(Rehearsal(date, 180))
        rh.registerScene("Scene 1")
        rh.updateSceneDuration("Scene 1", 30)
        val prod = ConvertToStorable.convert( production)
        assertEquals("Hamlet", prod.name)
        assertEquals("Hamlet", prod.parts[0].name)
        assertEquals(2, prod.parts.size)
        assertEquals("Scene 1", prod.scenes[0].name)
        assertEquals(2, prod.scenes.size)
        assertEquals("Scene 1", prod.rehearsals[0].scenes[0].scene)
        assertEquals(180, prod.rehearsals[0].duration)
        assertEquals(30, prod.rehearsals[0].scenes[0].actualDuration)
    }

    @Test
    fun testPartConversion(){
        val part = Part("Hamlet", "John")
        val converted = ConvertToStorable.convertPart(part)
        assertEquals("Hamlet", converted.name)
        assertEquals("John", converted.actor)
    }

    @Test
    fun testSceneConversion(){
        val scene = Scene("s1", duration = 10, parts = arrayOf(
            Selectable("Hamlet", false),
            Selectable("Ophelia", true)
        ))
        val converted = ConvertToStorable.convertScene(scene)
        assertEquals("s1", converted.name)
        assertEquals(10, converted.duration)
        assertEquals("Ophelia", converted.parts[0])
    }
}
