package storage

import util.DateDTO
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertFalse
import kotlin.test.assertTrue


class ConversionToModelTest {

    @Test
    fun convertToModel(){
        val date = DateDTO(2010, 1, 1)
        val p1 = JSONPart("Hamlet", "John")
        val p2 = JSONPart("Ophelia", "Mary")
        val scene = JSONScene("s1", duration = 10, parts = arrayOf("Hamlet"))
        val sh = JSONSceneRehearsal("s1", 30)
        val rehearsal = JSONRehearsal(date,180, arrayOf(sh), arrayOf("John"))
        val production = JSONProduction("Hamlet",
            parts = arrayOf(p1, p2),
            scenes = arrayOf(scene),
            settings = JSONSettings(rehearsalDuration = 180,
                rehearsalSpacing = 3,
                language = "en"),
            rehearsals = arrayOf(rehearsal)
        )
        val prod = ConvertToModel.convert(production)
        assertEquals("Hamlet", prod.name)
        assertEquals("Hamlet", prod.parts[0].name)
        assertEquals("Ophelia", prod.parts[1].name)
        assertEquals(2, prod.parts.size)
        val scene2 = prod.scenes[0]
        assertEquals("s1", scene2.name)
        assertTrue(scene2.isInScene("Hamlet"))
        assertFalse(scene2.isInScene("Ophelia"))
        assertEquals(10, scene2.duration)

        val rh = prod.rehearsalManager.rehearsalByDate(date)!!
        assertEquals(date, rh.date)
        assertEquals(180, rh.duration)
        assertTrue(rh.sceneIsRehearsed("s1"))
        assertEquals("John", prod.rehearsalManager.getAbsenteesForRehearsal(date)[0])

    }

}
